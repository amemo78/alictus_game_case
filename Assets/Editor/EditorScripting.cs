﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

public class EditorScripting : EditorWindow
{

    struct S_SELECTED_CELL
    {
        public int horizontal_index;
        public int vertical_index;
    }

    TextAsset textJson;
    Texture2D CherryTexture;
    Texture2D BananaTexture;
    Texture2D WatermelonTexture;
    GameObject cherry;
    GameObject banana;
    GameObject watermelon;
    bool changeObj;
    S_SELECTED_CELL s_selected_cell;
    StreamWriter streamWriter;

    [SerializeField]
    JsonDataStructure jsonData;
    int[,] _matrix_data;


    private void OnEnable()
    {
        GetAssetPreviews();
        jsonData = new JsonDataStructure();
    }


    [MenuItem("Window/Match-3 Scene Configuration")]
    public static void ShowWindow()
    {
        GetWindow(typeof(EditorScripting));
    }


    public void OnGUI()
    {
        GUILayout.Width(500);
        GUILayout.Height(500);

        GUILayout.Label("MATCH-3 PUZZLE CONFIG", EditorStyles.boldLabel);

        if (GUI.Button(new Rect(340, 20, 200, 100), "Read Matrix"))
        {
            GetJsonData();
        }

        if(GUI.Button(new Rect(620, 20, 200, 100), "Write Matrix"))
        {
            WriteJsonData();
        }

        if(_matrix_data != null)
        {
            CreateMatchPreview();
        }

        
    }

    void GetAssetPreviews()
    {
        cherry = Resources.Load("Match3/Cherry_m3") as GameObject;
        banana = Resources.Load("Match3/Banana_m3") as GameObject;
        watermelon = Resources.Load("Match3/Watermelon_m3") as GameObject;

        CherryTexture = AssetPreview.GetAssetPreview(cherry);
        BananaTexture = AssetPreview.GetAssetPreview(banana);
        WatermelonTexture = AssetPreview.GetAssetPreview(watermelon);

    }


    public void GetJsonData()
    {
        _matrix_data = new int[6, 5];
        textJson = Resources.Load("matrix") as TextAsset;
        string s = textJson.text;
        JsonUtility.FromJsonOverwrite(s, jsonData);
        Parse1dTo2dArray(jsonData._matrix, _matrix_data);
        
        CreateMatchPreview();
    }

    public void WriteJsonData()
    {
        Parse2dTo1dArray(jsonData._matrix, _matrix_data);
        string s = JsonUtility.ToJson(jsonData);
        streamWriter = new StreamWriter("Assets/Resources/matrix.txt");
        streamWriter.AutoFlush = true;
        streamWriter.WriteLine(s);
        streamWriter.Close();
        

    }

    public void CreateMatchPreview()
    {
        GetAssetPreviews();
        if (changeObj)
        {
            int selected = _matrix_data[s_selected_cell.vertical_index, s_selected_cell.horizontal_index];
            string[] options = new string[]
            {
                        "Cheery", "Banana", "Watermelon",
            };
            GUI.BeginGroup(new Rect(250, 0, 700, 300));
            GUIStyle gUIStyle = new GUIStyle();
            gUIStyle.fixedHeight = 200;
            gUIStyle.fixedWidth = 400;
            gUIStyle.margin = new RectOffset(200, 300, 150, 0);
            gUIStyle.fontStyle = FontStyle.Bold;
            gUIStyle.border = new RectOffset(0, 0, 0, 0);

            selected = EditorGUILayout.Popup("Change Object", selected, options, gUIStyle);
            GUI.EndGroup();

            _matrix_data[s_selected_cell.vertical_index, s_selected_cell.horizontal_index] = selected;
        }

        // Match-3 distribution field
        GUI.BeginGroup(new Rect(250, 300, 650, 770));


        GUI.Box(new Rect(0, 0, 650, 770), "Directions");

        for (int i = 0; i < 6; i++)
        {
            for (int j = 0; j < 5; j++)
            {
                Texture2D texture = null;
                switch (_matrix_data[i, j])
                {
                    case 0:
                        texture = CherryTexture;
                        break;
                    case 1:
                        texture = BananaTexture;
                        break;
                    case 2:
                        texture = WatermelonTexture;
                        break;
                    default:
                        Debug.LogError("Json data must only include 0,1 and 2 values !");
                        break;
                }
                if (texture == null)
                {
                    Debug.Log("texture hataaaaa !");
                    continue;
                }
                  
                int h = texture.height;
                int w = texture.width;
                if(GUI.Button(new Rect(j * w, i * h, w, h), texture))
                {
                    changeObj = true;
                    s_selected_cell.horizontal_index = j;
                    s_selected_cell.vertical_index = i;
                }
            }
        }

        GUI.EndGroup();

    }

    // Here if we use marshal copy , it will be faster than unsafe copy. But In editor time 30 byte copy is quite less memory transfer.
    void Parse1dTo2dArray(int[] one_dim, int[,] two_dim)
    {
        int[] local_one_dim = one_dim;
        int[,] local_two_dim = two_dim;

        if (local_one_dim.Length != local_two_dim.Length)
        {
            Debug.LogError("You can not copy the different length arrays to each other !");
            return;
        }

        for (int i = 0; i < local_two_dim.GetLength(0); i++)
        {
            for (int j = 0; j < local_two_dim.GetLength(1); j++)
            {
                _matrix_data[i, j] = local_one_dim[i * local_two_dim.GetLength(1) + j];
            }
        }

    }


    void Parse2dTo1dArray(int[] one_dim, int[,] two_dim)
    {
        int[] local_one_dim = one_dim;
        int[,] local_two_dim = two_dim;

        if (local_one_dim.Length != local_two_dim.Length)
        {
            Debug.LogError("You can not copy the different length arrays to each other !");
            return;
        }

        for (int i = 0; i < local_two_dim.GetLength(0); i++)
        {
            for (int j = 0; j < local_two_dim.GetLength(1); j++)
            {
                 jsonData._matrix[i * local_two_dim.GetLength(1) + j] = _matrix_data[i, j];
            }
        }
    }
}
