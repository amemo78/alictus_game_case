﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class JsonParser : MonoBehaviour
{
    TextAsset textJson;
    StreamWriter streamWriter;

    [SerializeField]
    JsonDataStructure jsonData;
    int[,] _matrix_data;
    // Start is called before the first frame update
    void Start()
    {
        jsonData = new JsonDataStructure();
        _matrix_data = new int[6, 5];
        textJson = Resources.Load("matrix") as TextAsset;
        ReadJsonData();
    }


    public void ReadJsonData()
    {
        string s = textJson.text;
        JsonUtility.FromJsonOverwrite(s, jsonData);
        Parse1dTo2dArray(jsonData._matrix, _matrix_data);
    }

    public int[,] GetJsonData()
    {
        return _matrix_data;
    }

    public int[,] GetJsonDataByReverse(int index1, int index2)
    {
        int item1 = _matrix_data[index1 / 5, index1 % 5];
        int item2 = _matrix_data[index2 / 5, index2 % 5];

        _matrix_data[index1 / 5, index1 % 5] = item2;
        _matrix_data[index2 / 5, index2 % 5] = item1;

        return _matrix_data;
    }

    public void WriteJsonData()
    {
        Parse2dTo1dArray(jsonData._matrix, _matrix_data);
        string s = JsonUtility.ToJson(jsonData);
        streamWriter = new StreamWriter("Assets/Resources/matrix.txt");
        streamWriter.AutoFlush = true;
        streamWriter.WriteLine(s);
        streamWriter.Close();

    }


    // Here if we use marshal copy , it will be faster than unsafe copy. But In editor time 30 byte copy is quite less for memory transfer operation.
    void Parse1dTo2dArray(int[] one_dim, int[,] two_dim)
    {
        int[] local_one_dim = one_dim;
        int[,] local_two_dim = two_dim;

        if (local_one_dim.Length != local_two_dim.Length)
        {
            Debug.LogError("You can not copy the different length arrays to each other !");
            return;
        }

        for (int i = 0; i < local_two_dim.GetLength(0); i++)
        {
            for (int j = 0; j < local_two_dim.GetLength(1); j++)
            {
                try
                {
                    _matrix_data[i, j] = local_one_dim[i * local_two_dim.GetLength(1) + j];
                }
                catch (System.Exception)
                {

                    throw;
                }

            }
        }

    }


    void Parse2dTo1dArray(int[] one_dim, int[,] two_dim)
    {
        int[] local_one_dim = one_dim;
        int[,] local_two_dim = two_dim;

        if (local_one_dim.Length != local_two_dim.Length)
        {
            Debug.LogError("You can not copy the different length arrays to each other !");
            return;
        }

        for (int i = 0; i < local_two_dim.GetLength(0); i++)
        {
            for (int j = 0; j < local_two_dim.GetLength(1); j++)
            {
                jsonData._matrix[i * local_two_dim.GetLength(1) + j] = _matrix_data[i, j];
            }
        }
    }
}
