/// <summary>
///  Globals carries all structure which is needed by manny different sides.
///  
///  created by: Ahmet �ent�rk
/// </summary>
namespace Globals
{
    public enum LevelType
    {
        Collection,
        Match,
        Match3,
        BrickBreaker
    }
    public enum ObjectType
    {
        Olive,
        Cherry,
        Banana,
        Hotdog,
        Hamburger,
        Cheese,
        Watermelon,
        Length
    }

    public enum Match3ObjectType
    {
        Cherry,
        Banana,
        Watermelon
    }

    public enum Match3Type
    {
        Horizontal,
        Vertical,
        NoMatch
    }
    public struct S_MATCH3_DATA
    {
        public int x_index;
        public int y_index;
        public Match3Type match3_type;
    }
    
}
