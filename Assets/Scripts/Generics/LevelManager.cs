using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Globals;

/// <summary>
///  LevelManager was designed to handle all level generation in most generic way. Here specific tasks are 
///  attached to level prefabs as scripts by level specific configuration.
///  It is responsible for creating level objects and it is triggered by game events in center and
///  they are evalutated and most of time it triggers something else.
///  created by: Ahmet �ent�rk
/// </summary>
public class LevelManager : MonoBehaviour
{
    public LevelType levelType;
    public ObjectType objControlledByPlayer;
    public List<GameObject> spawnedGameObjs;
    public List<GameObject> match3Objs;
    public bool creationCollisionProtection;
    public Vector2 planeBordersXZ;
    JsonParser jsonParser;
    int total_3_match_count;
    public PhysicMaterial bouncyMat;

    private void OnEnable()
    {
        EventController.onObjectsMatched += EvaluateNewMatch;
        EventController.onObjectDragged += Evaluate3Match;
    }

    private void OnDisable()
    {
        EventController.onObjectsMatched -= EvaluateNewMatch;
        EventController.onObjectDragged -= Evaluate3Match;
    }

    // Start is called before the first frame update
    void Start()
    {
        spawnedGameObjs = ObjectPooler.SharedInstance.pooledObjects;
        switch (levelType)
        {
            case LevelType.Collection:
                PlaceCollectionObjs();
                break;
            case LevelType.Match:
                PlaceMatchObjs();
                break;
            case LevelType.Match3:
                PlaceMatch3Objs();
                break;
            case LevelType.BrickBreaker:
                PlaceBrickBreakerObjs();
                break;
            default:
                break;
        }
    }

    void PlaceCollectionObjs()
    {
        match3Objs = new List<GameObject>();
        SceneRandomPlacement(creationCollisionProtection);
        AddMoverScript();
    }

    void PlaceMatchObjs()
    {
        SceneRandomPlacement(creationCollisionProtection);
        AddMatcherScript();
    }

    void PlaceMatch3Objs()
    {
        jsonParser = GameObject.Find("JsonParser").GetComponent<JsonParser>();
        SceneMatrixPlacment();
        AddDragDropScript();
        AddMatcher3Script();
    }

    void PlaceBrickBreakerObjs()
    {
        SetBrickPlacement();
    }

    void SceneRandomPlacement(bool collisionProtection)
    {
        if(collisionProtection)
        {
            // Here i am using predefined random distribution approach(4x8), so planeBordersXZ must be quite less in contrast to level1
            float obj_x_offset = 1.5f;
            float obj_z_offset = 1.2f;
            for (int i = 0; i < ObjectPooler.SharedInstance.itemsToPool[0].amountToPool; i++)
            {
                GameObject go = GetObj(ObjectType.Olive);
                Vector3 distribution_start_point = new Vector3(-obj_x_offset * 3 / 2, 0, -obj_z_offset * 6 / 2);
                go.transform.position = distribution_start_point +  new Vector3(obj_x_offset * (i%4) + Random.Range(-planeBordersXZ.x, planeBordersXZ.x), 0, obj_z_offset *  ((int)(i/4)) + Random.Range(-planeBordersXZ.y, planeBordersXZ.y));
                go.SetActive(true);
            }
        }
        else
        {
            for (int i = 0; i < ObjectPooler.SharedInstance.itemsToPool[0].amountToPool; i++)
            {
                GameObject go = GetObj(ObjectType.Olive);
                go.transform.position = new Vector3(Random.Range(-planeBordersXZ.x,planeBordersXZ.x), 0, Random.Range(-planeBordersXZ.y, planeBordersXZ.y));
                go.SetActive(true);
            }
        }
    }

    void SceneMatrixPlacment()
    {
        GameObject jsonParser = GameObject.Find("JsonParser");
        int[,] matrix_data = jsonParser.GetComponent<JsonParser>().GetJsonData();
        try
        {
            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    GameObject go = null;
                    switch ((Match3ObjectType)matrix_data[i, j])
                    {
                        case Match3ObjectType.Cherry:
                            go = GetObj(ObjectType.Cherry);
                            break;
                        case Match3ObjectType.Banana:
                            go = GetObj(ObjectType.Banana);
                            break;
                        case Match3ObjectType.Watermelon:
                            go = GetObj(ObjectType.Watermelon);
                            break;
                        default:
                            Debug.LogError("Matrix data must only include 0,1 and 2 values !");
                            break;
                    }
                    float obj_x_offset = 1.0f;
                    float obj_z_offset = 1.0f;
                    Vector3 distribution_start_point = new Vector3(-obj_x_offset * 4 / 2, 0, 5 + -obj_z_offset * 6 / 2);
                    go.GetComponent<Rigidbody>().useGravity = false;
                    go.GetComponent<Rigidbody>().isKinematic = false;
                    go.transform.position = distribution_start_point + new Vector3(obj_x_offset * j, 1, -obj_z_offset * i);
                    go.GetComponent<ObjectSpecs>().SetObjMatch3Index(i, j);
                    go.SetActive(true);
                    match3Objs.Add(go);
                    Debug.Log(i.ToString() + "-" + j.ToString());
                }
            }
        }
        catch (System.Exception e)
        {
            Debug.Log(e.Message);
        }
    }

    void SetBrickPlacement()
    {
        for (int i = 0; i < 7; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                GameObject go = GetRandomObj(ObjectType.Hotdog);

                if (go.GetComponent<ObjectSpecs>().objectType == ObjectType.Olive)
                {
                    ObjectPooler.SharedInstance.DestroyGameObj(go);
                    go = GetObj(ObjectType.Banana);
                }

                float obj_x_offset = 0.9f;
                float obj_z_offset = 0.9f;
                Vector3 distribution_start_point = new Vector3(-obj_x_offset * 7 / 2, 0, 9 + -obj_z_offset * 6 / 2);

                go.GetComponent<Rigidbody>().useGravity = false;
                go.GetComponent<Rigidbody>().isKinematic = true;
                go.transform.position = distribution_start_point + new Vector3(obj_x_offset * j, 0.5f, -obj_z_offset * i);
                go.GetComponent<ObjectSpecs>().SetObjMatch3Index(i, j);
                go.GetComponent<MeshCollider>().material = bouncyMat;
                go.AddComponent<BoxCollider>();
                go.SetActive(true);
                match3Objs.Add(go);
                Debug.Log(i.ToString() + "-" + j.ToString());
            }
        }
    }

    GameObject GetRandomObj(ObjectType exclude_type)
    {
        ObjectType random_type = (ObjectType)Random.Range(0, (int)ObjectType.Length);

        if (random_type == exclude_type)
        {
            if (exclude_type == ObjectType.Length - 1)
                random_type--;
            else
                random_type++;
        }
           
        return ObjectPooler.SharedInstance.GetPooledObject(random_type);
    }


    GameObject GetObj(ObjectType type)
    {
        return ObjectPooler.SharedInstance.GetPooledObject(type);
    }

    void AddMoverScript()
    {
        foreach  (GameObject go in spawnedGameObjs)
        {
            go.AddComponent<Mover>();
        }
    }

    void AddMatcherScript()
    {
        foreach (GameObject go in spawnedGameObjs)
        {
            go.AddComponent<Matcher>();
        }
    }

    void AddDragDropScript()
    {
        foreach (GameObject go in spawnedGameObjs)
        {
            go.AddComponent<DragDrop>();
        }
    }

    void AddMatcher3Script()
    {
        foreach (GameObject go in spawnedGameObjs)
        {
            go.AddComponent<Matcher3>();
        }
    }


    void EvaluateNewMatch(GameObject go1, GameObject go2)
    {
        if (go1.tag == "collected" || go2.tag == "collected")
            return;

        ObjectType type = go1.GetComponent<ObjectSpecs>().objectType; 

        int type_index = (int)type;
        type_index++;
        ObjectType upgraded_type = (ObjectType)type_index;
        GameObject upgraded_obj = GetObj(upgraded_type);
        upgraded_obj.transform.position = go1.transform.position;
        upgraded_obj.SetActive(true);

        upgraded_obj.GetComponent<ObjectSpecs>().objectType = upgraded_type;
        upgraded_obj.tag = "Untagged";

        go1.tag = "collected";
        go2.tag = "collected";
        ObjectPooler.SharedInstance.DestroyGameObj(go1);
        ObjectPooler.SharedInstance.DestroyGameObj(go2);

        if (type == ObjectType.Cheese)
        {
            EventController.LevelEnded();
            return;
        }

    }


    void Evaluate3Match(GameObject go, Vector2 dir)
    {
        int gameobject_index = go.GetComponent<ObjectSpecs>().GetObjMatch3StraightIndex();
        int pair_gameobject_index = go.GetComponent<Matcher3>().GetIxOnMoveDir(dir);

        if (pair_gameobject_index == - 1)
        {
            Debug.Log("Forbidden Move, do nothing.");
            return;
        }

        S_MATCH3_DATA match3_data = CheckMatch3Exists(jsonParser.GetJsonDataByReverse(gameobject_index, pair_gameobject_index), match3Objs[gameobject_index], match3Objs[pair_gameobject_index]);

        if (match3_data.match3_type == Match3Type.NoMatch)
        {
            // No match, revert the matrix.
            jsonParser.GetJsonDataByReverse(gameobject_index, pair_gameobject_index);
        }


    }


    S_MATCH3_DATA CheckMatch3Exists(int[,] matrix, GameObject go, GameObject go_pair)
    {
        S_MATCH3_DATA s_match3_data = new S_MATCH3_DATA();
        int match_count_in_one_move = 0;
        s_match3_data.match3_type = Match3Type.NoMatch;
        for (int i = 0; i < 6; i++)
        {
            for (int j = 0; j < 5; j++)
            {
                int mid_index = i * 5 + j;
                if ( i != 0 && i != 5)
                {
                    int a = matrix[i, j]; int b = matrix[i - 1, j]; int c = matrix[i + 1, j];

                    //Vertical check
                    if (a == b && a == c)
                    {
                        match_count_in_one_move++;
                        s_match3_data.match3_type = Match3Type.Vertical;
                        s_match3_data.x_index = i;
                        s_match3_data.y_index = j;
                        ProcessMatch3(Match3Type.Vertical, mid_index, go, go_pair);
                        continue;
                    }
                }

                if(j != 0 && j != 4)
                {
                    int d = matrix[i, j]; int e = matrix[i, j - 1]; int f = matrix[i, j + 1];
                    //Horizontal check
                    if (d == e && d == f)
                    {
                        match_count_in_one_move++;
                        s_match3_data.match3_type = Match3Type.Horizontal;
                        s_match3_data.x_index = i;
                        s_match3_data.y_index = j;
                        ProcessMatch3(Match3Type.Horizontal, mid_index, go, go_pair);
                    }
                }
            }
        }

        return s_match3_data;
    }

    void ProcessMatch3(Match3Type match3Type, int mid_go_index, GameObject go, GameObject go_pair)
    {
        if (!match3Objs[mid_go_index].activeInHierarchy)
            return;

        if(total_3_match_count % 2 != 1)
            ReplaceGameObjects(go, go_pair);

        if (match3Type == Match3Type.Vertical)
        {
            StartCoroutine(KillMatch3Objs(match3Objs[mid_go_index], 0.5f));
            StartCoroutine(KillMatch3Objs(match3Objs[mid_go_index - 5], 0.5f));
            StartCoroutine(KillMatch3Objs(match3Objs[mid_go_index + 5], 0.5f));
        }
        else if (match3Type == Match3Type.Horizontal)
        {
            StartCoroutine(KillMatch3Objs(match3Objs[mid_go_index], 0.5f));
            StartCoroutine(KillMatch3Objs(match3Objs[mid_go_index - 1], 0.5f));
            StartCoroutine(KillMatch3Objs(match3Objs[mid_go_index + 1], 0.5f));
        }
    }

    void ReplaceGameObjects(GameObject go1, GameObject go2)
    {
        Vector3 go1_pos = go1.transform.position;
        Vector3 go2_pos = go2.transform.position;

        int go1_index = go1.GetComponent<ObjectSpecs>().GetObjMatch3StraightIndex();
        int go2_index = go2.GetComponent<ObjectSpecs>().GetObjMatch3StraightIndex();

        int[] go1_match3_ix = go1.GetComponent<ObjectSpecs>().GetObjMatch3Index();
        int[] go2_match3_ix = go2.GetComponent<ObjectSpecs>().GetObjMatch3Index();

        go1.transform.position = go2_pos;
        go2.transform.position = go1_pos;

        go1.GetComponent<ObjectSpecs>().SetObjMatch3Index(go2_match3_ix[0], go2_match3_ix[1]);
        go1.GetComponent<ObjectSpecs>().SetObjMatch3Index(go1_match3_ix[0], go1_match3_ix[1]);
       
        GameObject temp = go1;
        match3Objs[go1_index] = go2;
        match3Objs[go2_index] = temp;

    }

    IEnumerator KillMatch3Objs(GameObject go, float time)
    {

        yield return new WaitForSeconds(time);
        if (go.activeInHierarchy)
        {
            total_3_match_count++;
            UIManager.inst.UpdateProgressBar((float)total_3_match_count / 30);
            ObjectPooler.SharedInstance.DestroyGameObj(go);
        }

        if (total_3_match_count == 30)
        {
            EventController.LevelEnded();
        }
    }

}
