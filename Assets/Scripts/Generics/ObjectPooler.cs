﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Globals;

/// <summary>
///  Pool-Object-Pattern provides that all game objects are created at initial. It is flexible as being expanded while necessary.
///  In run-time memory filling and deletion processes are blocked and memory spikes are destroyed.
///  
///  created by: Ahmet Şentürk
/// </summary>
public class ObjectPooler : MonoBehaviour
{
    public static ObjectPooler SharedInstance;

    public List<ObjectPoolItem> itemsToPool;

    public List<GameObject> pooledObjects;

    void Awake()
    {
        SharedInstance = this;
    }


    void Start()
    {
        pooledObjects = new List<GameObject>();
        foreach (ObjectPoolItem item in itemsToPool)
        {
            for (int i = 0; i < item.amountToPool; i++)
            {
                GameObject obj = (GameObject)Instantiate(item.objectToPool);
                obj.SetActive(false);
                pooledObjects.Add(obj);
                obj.GetComponent<ObjectSpecs>().SetObjIndex(i);
                if(SceneManager.GetActiveScene().name != "Level2" && SceneManager.GetActiveScene().name != "Level3")
                    obj.transform.localScale = obj.transform.localScale * 1.7f;
                else
                    obj.transform.localScale = obj.transform.localScale * 1.3f;
            }
        }
    }


    public GameObject GetPooledObject(ObjectType type)
    {
        for (int i = 0; i < pooledObjects.Count; i++)
        {
            if (!pooledObjects[i].activeInHierarchy && pooledObjects[i].GetComponent<ObjectSpecs>().objectType == type)
            {
                return pooledObjects[i];
            }
        }
        foreach (ObjectPoolItem item in itemsToPool)
        {
            if (item.objectToPool.GetComponent<ObjectSpecs>().objectType == type)
            {
                if (item.shouldExpand)
                {
                    GameObject obj = (GameObject)Instantiate(item.objectToPool);
                    obj.SetActive(false);
                    pooledObjects.Add(obj);
                    return obj;
                }
            }
        }
        return null;
    }

    public void DestroyGameObj(GameObject go)
    {
        go.SetActive(false);
    }

}
