using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Globals;


/// <summary>
///  GameManager was designed singleton and along the lifecycle it will be a initiator and finisher.
///  In first game launch initial datas were saved to memory and on next launches they are updated.
///  created by: Ahmet �ent�rk
/// </summary>

public class GameManager : MonoBehaviour
{

    public static GameManager instance;

    public static int level;
    [SerializeField]
    private int total_level;
    public int[] inventory;


    private void Awake()
    {

        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
            StartCoroutine(OpenCurrentScene());
#if !UNITY_ANDROID
            Application.targetFrameRate = 60;
#endif
        }
        else
        {
            Debug.LogError("GameManager s�n�f� singleton oldu�undan ba�ka bir instance olu�turamazs�n�z !");
        }

    }

    private void Start()
    {
        inventory = new int[(int)ObjectType.Length];
        ReadInventoryData();
    }

    public IEnumerator OpenCurrentScene()
    {
        if (PlayerPrefs.HasKey("level"))
        {
            level = PlayerPrefs.GetInt("level");
        }
        else
        {
            level = 1;
            PlayerPrefs.SetInt("level", level);
            InitInventoryData();
            PlayerPrefs.Save();
        }
        yield return new WaitForSeconds(0.1f);
        SceneManager.LoadSceneAsync(GetSceneIndex());
    }

    public void RestartLevel()
    {
        SceneManager.LoadSceneAsync(GetSceneIndex());        
    }


    public void GoNextLevel()
    {
        level++;

        PlayerPrefs.SetInt("level", level);
        PlayerPrefs.Save();
        SceneManager.LoadSceneAsync(GetSceneIndex());
    }

    private int GetSceneIndex()
    {
        int loop_level = level % total_level;
        if (loop_level == 0)
        {
            loop_level = total_level;
        }

        return loop_level;
    }

    private void InitInventoryData()
    {
        PlayerPrefs.SetInt("cherry_count", 0);
        PlayerPrefs.SetInt("banana_count", 0);
        PlayerPrefs.SetInt("hamburger_count", 0);
        PlayerPrefs.SetInt("cheese_count", 0);
        PlayerPrefs.SetInt("watermelon_count", 0);
        PlayerPrefs.SetInt("olive_count", 0);
        PlayerPrefs.SetInt("hotdog_count", 0);
    }

    public void SetInventoryData(ObjectType type)
    {

        switch (type)
        {
            case ObjectType.Cherry:
                PlayerPrefs.SetInt("cherry_count", inventory[(int)type] + 1);
                break;
            case ObjectType.Banana:
                PlayerPrefs.SetInt("banana_count", inventory[(int)type] + 1);
                break;
            case ObjectType.Hamburger:
                PlayerPrefs.SetInt("hamburger_count", inventory[(int)type] + 1);
                break;
            case ObjectType.Cheese:
                PlayerPrefs.SetInt("cheese_count", inventory[(int)type] + 1);
                break;
            case ObjectType.Watermelon:
                PlayerPrefs.SetInt("watermelon_count", inventory[(int)type] + 1);
                break;
            case ObjectType.Olive:
                PlayerPrefs.SetInt("olive_count", inventory[(int)type] + 1);
                break;
            case ObjectType.Hotdog:
                PlayerPrefs.SetInt("hotdog_count", inventory[(int)type] + 1);
                break;
            default:
                break;
        }
        PlayerPrefs.Save();
    }

    private void ReadInventoryData()
    {
        inventory[(int)ObjectType.Olive] = PlayerPrefs.GetInt("olive_count");
        inventory[(int)ObjectType.Banana] = PlayerPrefs.GetInt("banana_count");
        inventory[(int)ObjectType.Hotdog] = PlayerPrefs.GetInt("hotdog_count");
        inventory[(int)ObjectType.Hamburger] = PlayerPrefs.GetInt("hamburger_count");
        inventory[(int)ObjectType.Cherry] = PlayerPrefs.GetInt("cherry_count" );
        inventory[(int)ObjectType.Cheese] = PlayerPrefs.GetInt("cheese_count");
        inventory[(int)ObjectType.Watermelon] = PlayerPrefs.GetInt("watermelon_count");

    }

    public int[] GetInventoryData()
    {
        ReadInventoryData();
        return inventory;
    }

    private void OnApplicationQuit()
    {
        PlayerPrefs.Save();
    }

}
