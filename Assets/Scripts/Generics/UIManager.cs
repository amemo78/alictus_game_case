using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Globals;

/// <summary>
///  UIManager was designed singleton and along the scene lifecycle it controls ui elements input and outputs.
///  Level start and finish steps are handled here. UIManager is subscribed to some In-Game Events. 
///  created by: Ahmet �ent�rk
/// </summary>
public class UIManager : MonoBehaviour
{
    public static UIManager inst;
    public GameObject LevelFailed;
    public GameObject LevelCleared;
    public GameObject Confetti;
    public GameObject LevelText;
    public GameObject ProgressBar;
    public GameObject InventoryPanel;
    public List<GameObject> InventoryIcons;


    private void OnEnable()
    {
        EventController.onLevelEnded += Success;
        EventController.onLevelEnded += GiveRandomInventory;
        EventController.onLevelEnded += FullFillProgressBar;
        EventController.onLevelFailed += Fail;
    }

    private void OnDisable()
    {
        EventController.onLevelEnded -= Success;
        EventController.onLevelEnded -= GiveRandomInventory;
        EventController.onLevelEnded -= FullFillProgressBar;
        EventController.onLevelFailed -= Fail;
    }

    private void Awake()
    {
        if (inst == null)
            inst = this;
    }

    private void Start()
    {
        LevelText.GetComponent<Text>().text = "LEVEL " + GameManager.level.ToString();
    }

    public void GoNextLevel()
    {
        GameManager.instance.GoNextLevel();
    }

    public void RestartLevel()
    {
        GameManager.instance.RestartLevel();
    }

    public void UpdateProgressBar(float percentage)
    {
        float _rate = 1 - percentage;
        ProgressBar.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, _rate * 112);
    }

    public void FullFillProgressBar()
    {
        ProgressBar.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, 0);
    }

    public void OpenInventoryPanel()
    {
        InventoryPanel.SetActive(true);

        int[] inventory = GameManager.instance.GetInventoryData();

        if(inventory != null)
        {
            int index = 0;
            foreach (GameObject go in InventoryIcons)
            {
                if (inventory[index] != 0)
                {
                    Color temp_color = go.GetComponent<Image>().color;
                    temp_color.a = 1.0f;
                    go.GetComponent<Image>().color = temp_color;
                    go.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = inventory[index].ToString();
                    go.transform.GetChild(0).gameObject.SetActive(true);
                }
                index++;
            }
        }

    }

    public void CloseInventoryPanel()
    {
        InventoryPanel.SetActive(false);
    }

    public void GiveRandomInventory()
    {
        InventoryPanel.SetActive(true);
        StartCoroutine(OpenInventorContent());
    }

    IEnumerator OpenInventorContent()
    {
        int random_ix = Random.Range(0, (int)(ObjectType.Length - 1));
        int[] inventory = GameManager.instance.GetInventoryData();
        Color temp_color;

        if (inventory == null)
            yield return 0;

        int index = 0;
        foreach (GameObject go in InventoryIcons)
        {
            if (inventory[index] != 0)
            {
                temp_color = go.GetComponent<Image>().color;
                temp_color.a = 1.0f;
                go.GetComponent<Image>().color = temp_color;
                go.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = inventory[index].ToString();
                go.transform.GetChild(0).gameObject.SetActive(true);
            }
            index++;
            Debug.Log("inv : " + index.ToString());
        }

        yield return new WaitForSeconds(1.0f);
        InventoryIcons[random_ix].transform.GetChild(0).gameObject.SetActive(true);
        InventoryIcons[random_ix].transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = (inventory[random_ix] + 1).ToString();
        GameManager.instance.SetInventoryData((ObjectType)random_ix);
        InventoryIcons[random_ix].transform.GetChild(0).GetComponent<TextMeshProUGUI>().color = Color.blue;
        
        yield return new WaitForSeconds(1.0f);
        InventoryIcons[random_ix].transform.GetChild(0).GetComponent<TextMeshProUGUI>().color = Color.red;
        temp_color = InventoryIcons[random_ix].GetComponent<Image>().color;
        temp_color.a = 1.0f;
        InventoryIcons[random_ix].GetComponent<Image>().color = temp_color;

    }

    IEnumerator FailByTime(float t)
    {
        yield return new WaitForSeconds(t);
        LevelFailed.SetActive(true);
    }


    IEnumerator SuccessByTime(float t)
    {
        yield return new WaitForSeconds(0.5f);

        Confetti.SetActive(true);
        Confetti.GetComponent<ParticleSystem>().Play();

        yield return new WaitForSeconds(3.5f);

        LevelCleared.SetActive(true);
    }

    public void Fail()
    {
        StartCoroutine(FailByTime(1.0f));
    }

    public void Success()
    {
        StartCoroutine(SuccessByTime(2.0f));
    }


}