﻿using System;
using UnityEngine;

/// <summary>
///  EventController was designed with Observer Pattern rules. when added event were triggered, only somewhere interested with the event are notified.
///  By using c# Action property, one or more parameter was passed to subscribers was provided.
///  
///  created by: Ahmet Şentürk
/// </summary>
public class EventController
{

    //Level-1
    public static Action <GameObject> onObjectCollected;
    public static Action<GameObject> onObjectMoveEnded;

    //Level-2
    public static Action<GameObject, GameObject> onObjectsMatched;

    //Level-3
    public static Action<GameObject, Vector2> onObjectDragged;
    public static Action<GameObject> onObjectMatched3;

    //Level-4
    public static Action onPadDirectionChange;
    public static Action onPadLengthUp;
    public static Action onWobbling;
    public static Action onBallSpeeding;
    public static Action onBiggerBall;
 

    public static Action onLevelEnded;
    public static Action onLevelFailed;

    public static void ObjectCollected(GameObject go)
    {
        if (onObjectCollected != null)
        {
            onObjectCollected(go);
        }

    }

    public static void ObjectMoveEnded(GameObject go)
    {
        if (onObjectMoveEnded != null)
        {
            onObjectMoveEnded(go);
        }

    }

    public static void ObjectsMatched(GameObject go1, GameObject go2)
    {
        if (onObjectsMatched != null)
        {
            onObjectsMatched(go1, go2);
        }

    }

    public static void ObjectDragged(GameObject go, Vector2 dir)
    {
        if(onObjectDragged != null)
        {
            onObjectDragged(go, dir);
        }
    }

    public static void ObjectMatched3(GameObject go)
    {
        if(onObjectMatched3 != null)
        {
            onObjectMatched3(go);
        }
    }


    public static void PadDirectionReverse()
    {
        if(onPadDirectionChange != null)
        {
            onPadDirectionChange();
        }
    }

    public static void PadLengthUp()
    {
        if (onPadLengthUp != null)
        {
            onPadLengthUp();
        }
    }

    public static void BallWobbling()
    {
        if (onWobbling != null)
        {
            onWobbling();
        }
    }

    public static void BallSpeeding()
    {
        if (onBallSpeeding != null)
        {
            onBallSpeeding();
        }
    }

    public static void BiggerBall()
    {
        if (onBiggerBall != null)
        {
            onBiggerBall();
        }
    }

    public static void LevelFailed()
    {
        if (onLevelFailed != null)
        {
            onLevelFailed();
        }
    }

    public static void LevelEnded()
    {
        if (onLevelEnded != null)
        {
            onLevelEnded();
        }
    }

}
