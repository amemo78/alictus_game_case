﻿
using UnityEngine;
using UnityEngine.EventSystems;


/// <summary>
///  DragDrop script was created for implemeting unity IDragHandler, IBeginDragHandler and IEndDragHandler interfaces. These 
///  interfaces provide us using touch and mouse input in one module.
///  
///  created by: Ahmet Şentürk
/// </summary>

public struct S_DRAG_DATA
{
    public bool onDrag;
    public bool onEndDrag;
    public Vector3 worldPosBegin;
    public Vector3 worldPosCurrent;
    public Vector3 worldPosEnd;
    public Vector3 currentDirectionNorm;
    public Vector3 endDirectionNorm;
    public Vector3 currentDirection;
    public Vector3 endDirection;
    public float currentforce;
    public float endforce;
    public float angleZ;
    public float angleX;
}

[RequireComponent(typeof(Collider))]
public class DragDrop : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler
{

    float camObjDistance;
    public S_DRAG_DATA s_drag_data;
    public float directionThreshold = 0.001f;
    public float validAngleAbsValue = 0;


    private void Start()
    {
        camObjDistance = Vector3.Distance(transform.position, Camera.main.transform.position);
    }

    public S_DRAG_DATA GetDragData()
    {
        return s_drag_data;
    }

    public void SetEndDragFalse()
    {
        s_drag_data.onEndDrag = false;
    }


    public void OnBeginDrag(PointerEventData eventData)
    {
        s_drag_data.worldPosBegin = CalcWorldPos(eventData.position);
        s_drag_data.onEndDrag = false;
    }


    public void OnDrag(PointerEventData eventData)
    {

        s_drag_data.worldPosCurrent = CalcWorldPos(eventData.position);

        Vector3 direction = s_drag_data.worldPosCurrent - s_drag_data.worldPosBegin;

        s_drag_data.angleZ = CalcZAngle(-direction);

        if (direction.magnitude < directionThreshold || Mathf.Abs(s_drag_data.angleZ) > validAngleAbsValue)
        {
            s_drag_data.onDrag = false;
            return;
        }

        s_drag_data.onDrag = true;

        s_drag_data.currentDirectionNorm = direction.normalized;

        s_drag_data.currentforce = direction.magnitude;

        s_drag_data.currentDirection = direction;

        Debug.Log(direction.ToString());
    }



    public void OnEndDrag(PointerEventData eventData)
    {

        s_drag_data.onDrag = false;

        s_drag_data.onEndDrag = true;

        s_drag_data.worldPosEnd = CalcWorldPos(eventData.position);

        Vector3 direction = s_drag_data.worldPosEnd - s_drag_data.worldPosBegin;

        s_drag_data.endDirectionNorm = direction.normalized;

        s_drag_data.endforce = direction.magnitude;

        s_drag_data.endDirection = direction;

        EventController.ObjectDragged(gameObject, new Vector2(s_drag_data.endDirection.x, s_drag_data.endDirection.z));
    }

    private Vector3 CalcWorldPos(Vector3 inputPos)
    {
        Vector3 worldPos;
        camObjDistance = 16;
        inputPos.z = camObjDistance;
        worldPos = Camera.main.ScreenToWorldPoint(inputPos);
        return worldPos;
    }

    private float CalcZAngle(Vector3 dragDir)
    {       
        return Vector3.Angle(Vector3.forward, dragDir);
    }


}
