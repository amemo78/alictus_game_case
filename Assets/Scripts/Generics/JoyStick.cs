using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;




/// <summary>
///  JoyStick script was created for controlling touch and mouse inputs together in a cross-functional way.
///  
///  created by: Ahmet �ent�rk
/// </summary>
/// 

public class JoyStick : MonoBehaviour
{


    public static JoyStick inst;

    [HideInInspector] public Vector2 dir;
    [HideInInspector] public Vector2 deltaDir;
    [HideInInspector] public bool isTouchDown;
    [HideInInspector] public bool isTouchUp;
    [HideInInspector] public bool isTouching;
    public float dirMax = 6f;
    public float joystickScreenMult = 20f;

    private Vector2 dirOld;
    private const int NO_TOUCH = -1;
    private int touchId;
    private Vector2 joystickCenterPos;
    private bool touchControls;
    private Rigidbody rb;
    public float speed_mult = 1;


    private void Awake()
    {
        inst = this;
        touchId = NO_TOUCH;
        touchControls = Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.Android;

        rb = GetComponent<Rigidbody>();

    }

    private void Update()
    {
        int touchIdOld = touchId;
        dirOld = dir;

        if (touchControls)
        {
            foreach (Touch touch in Input.touches)
            {
                switch (touch.phase)
                {
                    case TouchPhase.Began:
                        if (touchId == NO_TOUCH && !IsPointerOverUIObject(touch.fingerId))
                        {
                            touchId = touch.fingerId;
                            joystickCenterPos = touch.position;
                        }
                        break;
                    case TouchPhase.Canceled:
                    case TouchPhase.Ended:
                        touchId = NO_TOUCH;
                        break;
                }
            }
        }
        else
        {
            if (Input.GetMouseButtonDown(0) && !IsPointerOverUIObject())
            {
                touchId = 0;
                joystickCenterPos = Input.mousePosition;
            }
            if (Input.GetMouseButtonUp(0))
            {
                touchId = NO_TOUCH;
            }
        }

        if (touchId != NO_TOUCH)
        {
            float multiplier = joystickScreenMult / Screen.width;
            dir = (GetTouchPos(touchId) - joystickCenterPos) * multiplier;
            float m = dir.magnitude;
            if (m > dirMax) dir = dir * dirMax / m;
            deltaDir = dir - dirOld;
            
            transform.LookAt(transform.position + new Vector3(dir.x, 0, dir.y));

        }
        else
        {
            dir = Vector2.zero;
            if (Input.GetAxisRaw("Horizontal") != 0f || Input.GetAxisRaw("Vertical") != 0f)
            {
                dir = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
                dir = dir.normalized * dirMax;
            }
            deltaDir = Vector2.zero;

        }

        isTouchDown = touchIdOld == NO_TOUCH && touchId != NO_TOUCH;
        isTouchUp = touchIdOld != NO_TOUCH && touchId == NO_TOUCH;
        isTouching = touchId != NO_TOUCH;


        if (dir.magnitude > 0.0001f)
        {
            rb.velocity = new Vector3(dir.x, 0, dir.y) * speed_mult;
        }
        else
        {
            rb.velocity = Vector3.zero;
        }
    }


    private Vector2 GetTouchPos(int touchId)
    {
        if (touchControls)
        {
            foreach (Touch touch in Input.touches)
            {
                if (touch.fingerId == touchId)
                {
                    return touch.position;
                }
            }
            return joystickCenterPos;
        }
        else
        {
            return Input.mousePosition;
        }
    }


    public bool IsPointerOverUIObject(int touchId = 0)
    {
        if (touchControls)
        {
            if (EventSystem.current.IsPointerOverGameObject(touchId)) return true;
        }
        else
        {
            if (EventSystem.current.IsPointerOverGameObject()) return true;
        }
        return false;
    }
}

