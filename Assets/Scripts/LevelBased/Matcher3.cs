﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Matcher3 : MonoBehaviour
{
    DragDrop dragDrop;
    S_DRAG_DATA s_drag_data;


    void Start()
    {
        dragDrop = GetComponent<DragDrop>();
        s_drag_data = dragDrop.GetDragData();
    }



    public int GetIxOnMoveDir(Vector2 dir)
    {
        int x_index = GetComponent<ObjectSpecs>().GetObjMatch3Index()[0];
        int y_index = GetComponent<ObjectSpecs>().GetObjMatch3Index()[1];

        int x_pair_index = x_index;
        int y_pair_index = y_index;

        if(Mathf.Abs(dir.x) > Mathf.Abs(dir.y))
        {
            //Horizontal Move
            if(dir.x > 0)
            {
                // Right Move
                if (y_index == 4)
                    return -1;
                y_pair_index = y_index + 1;

            }
            else
            {
                //Left Move
                if (y_index == 0)
                    return -1;
                y_pair_index = y_index - 1;
            }
        }
        else
        {
            //Vertical Move
            if (dir.y > 0)
            {
                // Upside Move
                if (x_index == 0)
                    return -1;
                x_pair_index = x_index - 1;
            }
            else
            {
                //Downside Move
                if (x_index == 5)
                    return -1;
                x_pair_index = x_index + 1;
            }
        }

        return x_pair_index * 5 + y_pair_index;
    }

    public void KillMatchedObjects(Globals.Match3Type match3Type)
    {
        if(match3Type == Globals.Match3Type.Horizontal)
        {

        }
        else if(match3Type == Globals.Match3Type.Vertical)
        {
            
        }
        else
        {
            Debug.LogError("Forbidden matchtype !");
        }
    }


}
