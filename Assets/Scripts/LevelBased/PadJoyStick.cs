﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PadJoyStick : MonoBehaviour
{
    public static PadJoyStick inst;

    [HideInInspector] public Vector2 dir;
    [HideInInspector] public Vector2 deltaDir;
    [HideInInspector] public bool isTouchDown;
    [HideInInspector] public bool isTouchUp;
    [HideInInspector] public bool isTouching;
    public float dirMax = 6f;
    public float joystickScreenMult = 20f;

    private Vector2 dirOld;
    private const int NO_TOUCH = -1;
    private int touchId;
    private Vector2 joystickCenterPos;
    private bool touchControls;
    private Rigidbody rb;
    public float zforce;
    private int direction_reverser = 1;
    float pad_x_max_distance;

    private void OnEnable()
    {
        EventController.onPadDirectionChange += PadDirectionReverse;
        EventController.onPadLengthUp += PadLengthUp;
    }

    private void OnDisable()
    {
        EventController.onPadDirectionChange -= PadDirectionReverse;
        EventController.onPadLengthUp -= PadLengthUp;
    }

    private void Awake()
    {
        inst = this;
        touchId = NO_TOUCH;
        touchControls = Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.Android;

        rb = GetComponent<Rigidbody>();
        pad_x_max_distance = 3.3f;
    }

    private void Update()
    {
        int touchIdOld = touchId;
        dirOld = dir;

        if (touchControls)
        {
            foreach (Touch touch in Input.touches)
            {
                switch (touch.phase)
                {
                    case TouchPhase.Began:
                        if (touchId == NO_TOUCH && !IsPointerOverUIObject(touch.fingerId))
                        {
                            touchId = touch.fingerId;
                            joystickCenterPos = touch.position;
                        }
                        break;
                    case TouchPhase.Canceled:
                    case TouchPhase.Ended:
                        touchId = NO_TOUCH;
                        break;
                }
            }
        }
        else
        {
            if (Input.GetMouseButtonDown(0) && !IsPointerOverUIObject())
            {
                touchId = 0;
                joystickCenterPos = Input.mousePosition;
            }
            if (Input.GetMouseButtonUp(0))
            {
                touchId = NO_TOUCH;
            }
        }

        if (touchId != NO_TOUCH)
        {
            float multiplier = joystickScreenMult / Screen.width;
            dir = (GetTouchPos(touchId) - joystickCenterPos) * multiplier;
            float m = dir.magnitude;
            if (m > dirMax) dir = dir * dirMax / m;
            deltaDir = dir - dirOld;


        }
        else
        {
            dir = Vector2.zero;
            if (Input.GetAxisRaw("Horizontal") != 0f || Input.GetAxisRaw("Vertical") != 0f)
            {
                dir = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
                dir = dir.normalized * dirMax;
            }
            deltaDir = Vector2.zero;

        }

        isTouchDown = touchIdOld == NO_TOUCH && touchId != NO_TOUCH;
        isTouchUp = touchIdOld != NO_TOUCH && touchId == NO_TOUCH;
        isTouching = touchId != NO_TOUCH;

        
        if (dir.magnitude > 0.0001f)
        {
            if (dir.x > 0)
            {
                rb.MovePosition(transform.position + Vector3.right * Time.deltaTime * 10 * direction_reverser);
                if (transform.position.x > pad_x_max_distance)
                    transform.position = new Vector3(pad_x_max_distance, transform.position.y, transform.position.z);
                if (transform.position.x < -pad_x_max_distance)
                    transform.position = new Vector3(-pad_x_max_distance, transform.position.y, transform.position.z);
            }
             
            else
            {
                rb.MovePosition(transform.position + Vector3.left * Time.deltaTime * 10 * direction_reverser);
                if (transform.position.x > pad_x_max_distance)
                    transform.position = new Vector3(pad_x_max_distance, transform.position.y, transform.position.z);
                if (transform.position.x < -pad_x_max_distance)
                    transform.position = new Vector3(-pad_x_max_distance, transform.position.y, transform.position.z);
            }
                
        }
        else
        {
            rb.velocity = Vector3.zero;
        }
    }


    private Vector2 GetTouchPos(int touchId)
    {
        if (touchControls)
        {
            foreach (Touch touch in Input.touches)
            {
                if (touch.fingerId == touchId)
                {
                    return touch.position;
                }
            }
            return joystickCenterPos;
        }
        else
        {
            return Input.mousePosition;
        }
    }

    public void PadDirectionReverse()
    {
        direction_reverser = -direction_reverser;
    }

    public void PadLengthUp()
    {
        transform.localScale = transform.localScale + Vector3.right * 0.1f;
    }


    public bool IsPointerOverUIObject(int touchId = 0)
    {
        if (touchControls)
        {
            if (EventSystem.current.IsPointerOverGameObject(touchId)) return true;
        }
        else
        {
            if (EventSystem.current.IsPointerOverGameObject()) return true;
        }
        return false;
    }
}

