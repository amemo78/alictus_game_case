using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Globals;

public class ObjectSpecs : MonoBehaviour
{
    public ObjectType objectType;
    private int index;
    public int[] match3_index;

    public void SetObjIndex(int ix)
    {
        index = ix;
    }

    public int GetObjIndex()
    {
        return index;
    }

    public void SetObjMatch3Index(int x, int y)
    {
        match3_index = new int[2];
        match3_index[0] = x;
        match3_index[1] = y;
    }

    public int[] GetObjMatch3Index()
    {
        return match3_index;
    }

    public int GetObjMatch3StraightIndex()
    {
        return match3_index[0] * 5 + match3_index[1];
    }
}
