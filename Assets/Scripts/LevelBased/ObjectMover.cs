using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectMover : MonoBehaviour
{
    public Transform referenceTr;
    private void OnEnable()
    {
        EventController.onObjectCollected += MoveObject;
    }

    private void OnDisable()
    {
        EventController.onObjectCollected -= MoveObject;
    }


    private void MoveObject(GameObject go)
    {
        go.GetComponent<Rigidbody>().isKinematic = true;
        go.GetComponent<Rigidbody>().useGravity = false;
        go.GetComponent<Mover>().SetNewPosbyLerp(referenceTr.position);
    }

}
