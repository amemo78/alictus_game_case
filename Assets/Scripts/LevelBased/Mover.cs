using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Mover : MonoBehaviour
{
    public Vector3 pos;
    private bool move;
    public float collectionSpeed = 5.0f;
    public const float epsilon = 0.01f;


    void Update()
    {
        if(move)
        {
            transform.position = Vector3.Lerp(transform.position, pos, Time.deltaTime * collectionSpeed);
            if(Vector3.Distance(transform.position, pos) < epsilon)
            {
                EventController.ObjectMoveEnded(gameObject);
                move = false;
            }
        }
    }

    public void SetNewPosbyLerp(Vector3 pos)
    {
        this.pos = pos + new Vector3(Random.Range(-0.3f, 0.3f), Random.Range(-0.2f, 0.2f), Random.Range(-0.3f, 0.3f));
        move = true;
    }


}
