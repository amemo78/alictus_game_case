﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Globals;

public class Breaker : MonoBehaviour
{

    Rigidbody rb;
    public Vector3 initial_force;
    public List<Transform> wobbling_tr;
    private Vector3 initial_euler_angles;
    private Vector3 velocity;
    float speed_mult = 1;
    int total_collected_items;

    public enum WobblingState
    {
        NoWobbling,
        Right,
        Left
    }

    WobblingState wobblingState;

    private void OnEnable()
    {
        EventController.onWobbling += Wobble;
        EventController.onBallSpeeding += SpeedUp;
        EventController.onBiggerBall += SizeUp;
    }

    private void OnDisable()
    {
        EventController.onWobbling -= Wobble;
        EventController.onBallSpeeding -= SpeedUp;
        EventController.onBiggerBall -= SizeUp;
    }

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        initial_euler_angles = transform.eulerAngles;
        StartCoroutine(StartLevel(0.5f));
        wobblingState = WobblingState.NoWobbling;

    }

    private void Update()
    {

        switch (wobblingState)
        {
            case WobblingState.NoWobbling:
                break;
            case WobblingState.Right:
                transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, wobbling_tr[0].eulerAngles, Time.deltaTime * 5);
                if (Vector3.Distance(transform.eulerAngles, wobbling_tr[0].eulerAngles) < 0.1f)
                    wobblingState = WobblingState.Left;
                break;
            case WobblingState.Left:
                transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, wobbling_tr[1].eulerAngles, Time.deltaTime * 5);
                if (Vector3.Distance(transform.eulerAngles, wobbling_tr[1].eulerAngles) < 0.1f)
                    wobblingState = WobblingState.Right;
                break;
            default:
                break;
        }


    }



    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "fail_bar")
        {
            EventController.LevelFailed();
            rb.velocity = Vector3.zero;
            return;
        }

        StartCoroutine(ProtectKineticenergy());

        if (collision.gameObject.GetComponent<ObjectSpecs>() == null)
            return;

        ObjectType type = collision.gameObject.GetComponent<ObjectSpecs>().objectType;

        switch (type)
        {

            case ObjectType.Cherry:
                EventController.BallSpeeding();
                break;
            case ObjectType.Banana:
                EventController.PadLengthUp();
                break;
            case ObjectType.Hamburger:
                EventController.PadDirectionReverse();
                break;
            case ObjectType.Cheese:
                EventController.BallWobbling();
                break;
            case ObjectType.Watermelon:
                EventController.BiggerBall();
                break;
            default:
                break;
        }

        if(type != ObjectType.Hotdog && collision.gameObject.tag != "collected")
        {
            ObjectPooler.SharedInstance.DestroyGameObj(collision.gameObject);
            total_collected_items++;
            UIManager.inst.UpdateProgressBar((float)total_collected_items / 56);
            if(total_collected_items == 56)
            {
                EventController.LevelEnded();
            }
        }

        collision.gameObject.tag = "collected";

    }


    IEnumerator StartLevel(float time)
    {
        yield return new WaitForSeconds(time);
        rb.AddForce(initial_force, ForceMode.Impulse);
    }


    private void Wobble()
    {
        //wobblingState = WobblingState.Right;
    }

    private void SpeedUp()
    {
        speed_mult += 0.1f;
    }

    private void SizeUp()
    {
        transform.localScale = transform.localScale + Vector3.one * 0.2f;
    }

    IEnumerator ProtectKineticenergy()
    {
        yield return new WaitForSeconds(0.1f);

        if (rb.velocity.normalized.x > 0 && rb.velocity.normalized.z > 0)
        {
            //Right-Top
            velocity = new Vector3(1, 0, 1) * speed_mult;
        }
        if (rb.velocity.normalized.x > 0 && rb.velocity.normalized.z < 0)
        {
            //Right-Down
            velocity = new Vector3(1, 0, -1) * speed_mult;
        }
        if (rb.velocity.normalized.x < 0 && rb.velocity.normalized.z > 0)
        {
            //Left-Top
            velocity = new Vector3(-1, 0, 1) * speed_mult;
        }
        if (rb.velocity.normalized.x < 0 && rb.velocity.normalized.z < 0)
        {
            //Left-Down
            velocity = new Vector3(-1, 0, -1) * speed_mult;
        }

        rb.AddForce(velocity * 0.015f, ForceMode.Impulse);
    }
}
