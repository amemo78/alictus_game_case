using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Globals;

public class CollectionField : MonoBehaviour
{
    public List<GameObject> collectedObjs;
    public Transform referenceTr;
    public float collectSpeed;
    private const float epsilon = 0.01f;
    private const int collectedLayerIndex = 6;
    // Start is called before the first frame update
    void Start()
    {
        collectedObjs = new List<GameObject>();
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<ObjectSpecs>() == null || other.gameObject.tag == "collected")
            return;

        if(other.gameObject.GetComponent<ObjectSpecs>().objectType == ObjectType.Olive)
        {
            collectedObjs.Add(other.gameObject);
            other.gameObject.tag = "collected";
            other.gameObject.layer = collectedLayerIndex;
            EventController.ObjectCollected(other.gameObject);
            UIManager.inst.UpdateProgressBar((float)collectedObjs.Count / 64);
            if(collectedObjs.Count == 64)
            {
                EventController.LevelEnded();
            }
        }
    }


}
