using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Matcher : MonoBehaviour
{

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<ObjectSpecs>() == null || collision.gameObject.tag == "collected")
            return;

        if(collision.gameObject.GetComponent<ObjectSpecs>().objectType == gameObject.GetComponent<ObjectSpecs>().objectType)
        {
            //there is a match
            EventController.ObjectsMatched(gameObject, collision.gameObject);
        }
    }

}
